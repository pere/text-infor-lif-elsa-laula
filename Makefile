
SOURCE = manuscript

GENERATED = \
  $(SOURCE).pdf $(SOURCE).epub $(SOURCE).html \
  $(SOURCE).en.pdf $(SOURCE).en.epub $(SOURCE).en.html \
  $(SOURCE).nb.pdf $(SOURCE).nb.epub $(SOURCE).nb.html \
  $(SOURCE).se.pdf $(SOURCE).se.epub $(SOURCE).se.html \
  $(SOURCE).sma.pdf $(SOURCE).sma.epub $(SOURCE).sma.html

DBLATEX = dblatex

DBLATEX_OPTS = \
	-T simple \
	-b xetex \
	-r data/dblatex-postprocess \
	--xsl-user=data/user_param.xsl \
	--xsl-user=data/xetex_param.xsl \
	-S data/dblatexconfig.xml \
	-V \
	-p data/pdf.xsl

all: $(GENERATED)

$(SOURCE).xml: *.adoc $(SOURCE)-docinfo*.xml
	asciidoctor -b docbook5 -d book $(SOURCE).adoc --out-file=$@
$(SOURCE).pdf: $(SOURCE).xml
	$(DBLATEX) $(DBLATEX_OPTS) $<
$(SOURCE).epub: $(SOURCE).xml
	dbtoepub $^

$(SOURCE).html: $(SOURCE).xml
	xsltproc  --encoding UTF-8 \
	    --output $@ \
	    data/stylesheet-html.xsl \
	    $<

$(SOURCE)-fop.fo: $(SOURCE).xml
	xsltproc  \
	    --output $(subst .pdf,.fo,$@).new \
	    data/stylesheet-fo.xsl \
	    $<
	xmllint --format $@.new > $@
	$(RM) $@.new

$(SOURCE)-fop.pdf: $(SOURCE)-fop.fo
	fop -c data/fop-params.xconf -fo $(subst .pdf,.fo,$@) -pdf $@ ; \

po/$(SOURCE).pot: $(SOURCE).xml
	po4a --no-translations po4a.cfg
po/%.po: po/$(SOURCE).pot
	po4a --no-translations --msgmerge-opt --no-location po4a.cfg

$(SOURCE).en.xml: po/$(SOURCE).en.po $(SOURCE).xml
	po4a --translate-only $(SOURCE).en.xml-raw po4a.cfg
	sed -e 's%</chapter>%</chapter>\n<xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="manuscript.xml" xpointer="_inför_lif_eller_död"/>%' -e 's%xml:id="%xml:id="en-%' < $(SOURCE).en.xml-raw > $(SOURCE).en.xml.new && mv $(SOURCE).en.xml.new $(SOURCE).en.xml
$(SOURCE).en.pdf: $(SOURCE).en.xml Makefile data/pdf.xsl
	dblatex $(DBLATEX_OPTS) $(SOURCE).en.xml -o $@
$(SOURCE).en.epub: $(SOURCE).en.xml
	dbtoepub $^
$(SOURCE).en.html: $(SOURCE).en.xml
	xsltproc  --encoding UTF-8 \
	    --output $@ \
	    data/stylesheet-html.xsl \
	    $<

$(SOURCE).nb.xml: po/$(SOURCE).nb.po $(SOURCE).xml
	po4a --translate-only $(SOURCE).nb.xml-raw po4a.cfg
	sed -e 's%</chapter>%</chapter>\n<xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="manuscript.xml" xpointer="_inför_lif_eller_död"/>%' -e 's%xml:id="%xml:id="nb-%' < $(SOURCE).nb.xml-raw > $(SOURCE).nb.xml.new && mv $(SOURCE).nb.xml.new $(SOURCE).nb.xml
$(SOURCE).nb.pdf: $(SOURCE).nb.xml Makefile data/pdf.xsl
	dblatex $(DBLATEX_OPTS) $(SOURCE).nb.xml -o $@
$(SOURCE).nb.epub: $(SOURCE).nb.xml
	dbtoepub $^
$(SOURCE).nb.html: $(SOURCE).nb.xml
	xsltproc  --encoding UTF-8 \
	    --output $@ \
	    data/stylesheet-html.xsl \
	    $<
$(SOURCE).nb.txt: $(SOURCE).nb.html
	w3m -dump $(SOURCE).nb.html > $(SOURCE).nb.txt.new && mv $(SOURCE).nb.txt.new  $(SOURCE).nb.txt

$(SOURCE).se.xml: po/$(SOURCE).se.po $(SOURCE).xml
	po4a --translate-only $(SOURCE).se.xml-raw po4a.cfg
	sed -e 's%</chapter>%</chapter>\n<xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="manuscript.xml" xpointer="_inför_lif_eller_död"/>%' -e 's%xml:id="%xml:id="se-%' < $(SOURCE).se.xml-raw > $(SOURCE).se.xml.new && mv $(SOURCE).se.xml.new $(SOURCE).se.xml
$(SOURCE).se.pdf: $(SOURCE).se.xml Makefile data/pdf.xsl
	dblatex $(DBLATEX_OPTS) $(SOURCE).se.xml -o $@
$(SOURCE).se.epub: $(SOURCE).se.xml
	dbtoepub $^
$(SOURCE).se.html: $(SOURCE).se.xml
	xsltproc  --encoding UTF-8 \
	    --output $@ \
	    data/stylesheet-html.xsl \
	    $<

$(SOURCE).sma.xml: po/$(SOURCE).sma.po $(SOURCE).xml
	po4a --translate-only $(SOURCE).sma.xml-raw po4a.cfg
	sed -e 's%</chapter>%</chapter>\n<xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="manuscript.xml" xpointer="_inför_lif_eller_död"/>%' -e 's%xml:id="%xml:id="sma-%' < $(SOURCE).sma.xml-raw > $(SOURCE).sma.xml.new && mv $(SOURCE).sma.xml.new $(SOURCE).sma.xml
$(SOURCE).sma.pdf: $(SOURCE).sma.xml Makefile data/pdf.xsl
	dblatex $(DBLATEX_OPTS) $(SOURCE).sma.xml -o $@
$(SOURCE).sma.epub: $(SOURCE).sma.xml
	dbtoepub $^
$(SOURCE).sma.html: $(SOURCE).sma.xml
	xsltproc  --encoding UTF-8 \
	    --output $@ \
	    data/stylesheet-html.xsl \
	    $<

clean:
	$(RM) *~ */*~ manuscript.*.xml-raw
distclean:
	$(RM) $(GENERATED) manuscript.*.xml
